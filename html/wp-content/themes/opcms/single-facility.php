<?php $this_pagetitle = single_post_title('', false); ?>
<?php get_header(); ?>

  <div id="wrapper">
    <div id="wrapper-in">

      <nav>
        <ul id="bcarea" class="pc">
          <li><a href="<?php home_url('/'); ?>">top</a>&nbsp;&gt;&nbsp;</li><!--
          --><li><a href="<?php home_url('/'); ?>bath">施設のご案内</a>&nbsp;&gt;&nbsp;<li><!--
          --><li><?php echo $this_pagetitle; ?>
        </ul>
      </nav>

      <main>
        <div id="main" class="akasuri low-fac">
          <div id="lnav" class="pc">
            <nav>
              <ul>
                <li><a href="<?php echo home_url('/'); ?>hogushidoko/">ボディーケアほぐし処</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>akasuri/">本場韓国式アカスリ･エステ</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>bali/">バリ式エステ</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>ganbanyoku/">岩陶温熱 崋蒸洞</a></li>
              </ul>
            </nav>
          </div><!-- /#lnav -->


<?php if (is_object_in_term($post->ID, 'facilitycategory','akasuri')): ?>

          <section>
            <div id="section01" class="section">
              <h3 class="h3ttl"><span>本場韓国式アカスリ･エステ</span></h3>
              <div class="exparea">
                <p class="img"><img src="[thissiteurl]common/img/akasuri/img01.jpg" alt="本場韓国式アカスリ･エステの写真" width="299" height="210"></p>
                <div class="txt">
                  <p>本場韓国式アカスリは、お風呂やサウナで十分にからだを温めていただき毛穴が開いた状態でおこなうので、体の古い角質や毛穴に詰まった老廃物をきれいに取り除き、お肌をスベスベにするとともに、新陳代謝を高め、血行の促進を促します。また全身オイルトリートメントや塩もみ等と一緒に行いますと、日常のストレスや疲れが癒されリラクゼーション効果が得られます。是非お風呂に入った際にお試しください。<br>和の湯、洋の湯共にございます。</p>
                </div><!-- /.txt -->
              </div><!-- /.exparea -->

              <table class="menu-tbl mb">
                <caption>韓国式アカスリ</caption>
                <tr>
                  <th>メニュー</th>
                  <th>料金</th>
                </tr>
                <tr>
                  <td>A.アカスリコース<br><span>全身アカスリ＋ボディトリートメント</span></td>
                  <td>30分　3,700円</td>
                </tr>
                <tr>
                  <td>B.さっぱりコース<br><span>アカスリ＋ボディトリートメント＋全身漢方リンパマッサージ</span></td>
                  <td>40分　5,400円 </td>
                </tr>
                <tr>
                  <td>C.コルギアカスリコース<br><span>アカスリ＋ボディトリートメント＋コルギ（美顔骨気）マッサージ ＋韓流パック</span></td>
                  <td>50分　7,000円</td>
                </tr>
                <tr>
                  <td>D.崋の湯コース（おすすめ）<br><span>アカスリ＋ボディトリートメント＋コルギ（美顔骨気）マッサージ＋韓流パック＋全身漢方リンパマッサージ＋ヘアートリートメント</span></td>
                  <td>60分　8,800円</td>
                </tr>
                <tr>
                  <td>E.スペシャルコース<br><span>アカスリ＋ボディトリートメント＋コルギ（美顔骨気）マッサージ＋韓流パック＋全身漢方リンパマッサージ＋スチームタオル首・肩・背中マッサージ＋ヘアートリートメント＋カカト角質ケア</span></td>
                  <td>80分　11,800円</td>
                </tr>
                <tr>
                  <td>Ｆ.デラックスコース<br><span>アカスリ＋ボディトリートメント＋コルギ（美顔骨気）マッサージ＋韓流パック＋全身漢方リンパマッサージ＋スチームタオル全身マッサージ＋美BODY漢方パック＋ヘアートリートメント＋カカト角質ケア</span></td>
                  <td>100分  14,200円</td>
                </tr>
              </table>

              <table class="menu-tbl">
                <caption>韓国式漢方エステ</caption>
                <tr>
                  <th>メニュー</th>
                  <th>料金</th>
                </tr>
                <tr>
                  <td>Ｇ.コルギ小顔フェイシャル<br><span>ピーリング＋コルギ（美顔骨気）マッサージ＋韓流パック</span></td>
                  <td>30分　4,200円</td>
                </tr>
                <tr>
                  <td>Ｈ.全身漢方エステコース<br><span>全身スリミング純金オイルマッサージ＋美BODY漢方パック<br>※美肌・アンチエイジング効果の高い漢方リンパマッサージで全身を引き締めます！</span></td>
                  <td>40分　6,200円</td>
                </tr>
                <tr>
                  <td>Ｉ.全身漢方エステコース<br><span>アカスリ＋ボディトリートメント＋コルギ（美顔骨気）マッサージ ＋韓流パック</span></td>
                  <td>60分　9,200円</td>
                </tr>
                <tr>
                  <td>Ｊ.コルギ漢方エステコース<br><span>クレンジング＋コルギ（美顔骨気）マッサージ＋韓流パック＋全身スリミング純金オイルマッサージ＋美BODY漢方パック<br>※ボディからフェイスまで全身漢方エステでケアできる漢　方満喫コース</span></td>
                  <td>80分　12,900円</td>
                </tr>
                <tr>
                  <td>E.スペシャルコース<br><span>アカスリ＋ボディトリートメント＋コルギ（美顔骨気）マッサージ＋韓流パック＋全身漢方リンパマッサージ＋スチームタオル首・肩・背中マッサージ＋ヘアートリートメント＋カカト角質ケア</span></td>
                  <td>80分　11,800円</td>
                </tr>
                <tr>
                  <td>Ｋ.カカト角質ケア<br>（オプション）全コースにプラスできます<br><span>つるつるカカトに！<br>※単品のみの施術はできません</span></td>
                  <td>10分　1,080円</td>
                </tr>
              </table>
            </div><!-- /#section01.section -->
          </section>

          <section>
            <div id="section02" class="section">
              <h3 class="h3ttl"><span><span class="pc">ボディーケアほぐし処　</span>キャンペーンのお知らせ</span></h3>
              <div id="fac-news">

<?php
$args = array(/* 配列に複数の引数を追加 */
  'post_type' => 'mytype', /*複数ある際はarray('','')と記述*/
  'taxonomy' => 'mycategory',
  'term' => 'myterm',
  'posts_per_page' => 1, /* 表示するページ数 */
); ?>

<?php
  $my_posts = get_posts( $args ); //クエリの指定 $posts = とは書かない
  global $post; //グローバル変数から値を取得
  foreach ( $my_posts as $post ) : // $my_postsを$postへ代入 $postにしないとthe_titleが取得できない
  setup_postdata( $post ); // ここからループ開始処理
?>
                <article>
                  <div class="box">
                    <h4><?php the_title(); ?></h4>
                    <div class="entry-body">
<?php the_field('facility_campaign') ?>
                    </div><!-- /.entry-body -->
                  </div><!-- /.box -->
                </article>

<?php
  endforeach; // ここまでサブループ。投稿がまだある場合は◯行目に戻る。endforeachに注意
  wp_reset_postdata(); //忘れずにリセットする必要がある
?>

<?php endif; ?>

         </div><!-- /#main -->
      </main>

<?php get_sidebar(); ?>

    </div><!-- /#wrapper-in -->
  </div><!-- /#wrapper -->

<?php get_footer(); ?>