<?php $this_pagetitle = get_post_type_object(get_post_type())->label; ?>
<?php get_header(); ?>

  <div id="wrapper">
    <div id="wrapper-in">

      <nav>
        <ul id="bcarea" class="pc"><!--
          --><li><a href="<?php echo home_url('/'); ?>">top</a>&nbsp;&gt;&nbsp;</li><!--
          --><li><?php echo $this_pagetitle; ?></li><!--
        --></ul>
      </nav>

      <main>
        <div id="main" class="price">
          <section>
            <div id="section01" class="section">
              <h3>料金のご案内</h3>
<?php
  $args = array(
    'post_type' => 'price',
    'taxonomy' => 'pricecategory',
    'term' => 'guide',
    'posts_per_page' => 1,
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
<?php the_field('price_table'); ?>

<?php
  endforeach;
  wp_reset_postdata();
?>
              <p>※&nbsp;&nbsp;岩盤浴は大人（中学生以上）のご利用とさせていただいております</p>
              <p>※&nbsp;&nbsp;岩盤浴料には専用着・マットを含みます。</p>
            </div><!--/#section01-->
          </section>

          <section>
            <div id="section02" class="section">
              <h3>お得なセット券と回数券</h3>
              <section>
                <h4>セット券</h4>
                <div class="txt_area">
                  <p>入浴プラスαで通常よりも安くご利用いただけるセット券をご用意しています。</p>
                  <p>ご利用時間：平日 10:00～15:00</p>
                </div><!--/.txt_area-->
                <div class="tbl_wrap02">
<?php
  $args = array(
    'post_type' => 'price',
    'taxonomy' => 'pricecategory',
    'term' => 'set',
    'posts_per_page' => 1,
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
<?php the_field('price_table'); ?>

<?php
  endforeach;
  wp_reset_postdata();
?>
                </div><!--/.tbl_wrap-->
                <p class="txt pc">※&nbsp;&nbsp;年末年始・ＧＷ・お盆休み期間はセット券の販売はお休みさせていただいております。</p>
              </section>

              <section>
                <h4>回数券</h4>
                <div class="txt_area">
                  <p>入浴プラスαで通常よりも安くご利用いただけるセット券をご用意しています。 （平日　am10:00～ｐｍ15:00）</p>
                </div><!--/.txt_area-->
                <div class="tbl_wrap01">
<?php
  $args = array(
    'post_type' => 'price',
    'taxonomy' => 'pricecategory',
    'term' => 'coupon',
    'posts_per_page' => 1,
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
<?php the_field('price_table'); ?>

<?php
  endforeach;
  wp_reset_postdata();
?>
                </div><!--/.tbl_wrap-->
                <p>※&nbsp;&nbsp;岩盤欲は大人（中学生以上）のご利用とさせて頂いております。</p>
                <p>※&nbsp;&nbsp;岩盤浴料には専用着・マットを含みます。</p>
              </section>
            </div><!--/#section02-->
          </section>

          <section>
            <div id="section03" class="section">
              <h3>キャンペーン</h3>
<?php
  $args = array(
    'post_type' => 'price',
    'taxonomy' => 'pricecategory',
    'term' => 'campaign',
    'posts_per_page' => 1,
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
              <article>
                <h4><?php the_title(); ?></h4>
                <div class="txt_area">
                  <p><?php the_field('price_campaign'); ?></p>
<?php
    $attachment_id = get_field('price_img');
    $size = "cs_price_img"; // (thumbnail, medium, large, full or custom size)
    $image = wp_get_attachment_image_src( $attachment_id, $size );
    $attachment = get_post( get_field('price_img') );
    $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
    $image_title = $attachment->post_title;
?>
<?php $imgid = get_field('price_img'); ?>
<?php if(!empty($imgid)):?>
                  <p><img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" /></p>
<?php else: endif; ?>
                </div><!--/.txt_area-->
              </article>
<?php
  endforeach;
  wp_reset_postdata();
?>
            </div><!--/#section03-->
          </section>
        </div><!-- /#main -->
      </main>

<?php get_sidebar(); ?>

    </div><!-- /#wrapper-in -->
  </div><!-- /#wrapper -->

<?php get_footer(); ?>