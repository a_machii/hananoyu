  <footer>
    <div id="footer">
      <div class="inner">
        <p class="to_top"><a href="#header" class="ophv"><img src="<?php echo home_url('/'); ?>common/img/to_top_btn.png" height="72" width="72" alt="page top"></a></p>
        <p class="pc"><img src="<?php echo home_url('/'); ?>common/img/f_banner.jpg" height="167" width="1002" alt="華の湯"></p>
        <p class="rsp"><img src="<?php echo home_url('/'); ?>common/img/rsp/f_banner.jpg" height="165" width="671" alt="華の湯"></p>
        <div class="unit">
          <ul class="pc">
            <li><a href="<?php echo home_url('/'); ?>">ホーム</a></li>
            <li><a href="<?php echo home_url('/'); ?>bath/">お風呂のご案内</a></li>
            <li><a href="<?php echo home_url('/'); ?>bath/wanoyu/">和の湯</a></li>
            <li><a href="<?php echo home_url('/'); ?>bath/yonoyu/">洋の湯</a></li>
            <li><a href="<?php echo home_url('/'); ?>facility/">施設のご案内</a></li>
            <li><a href="<?php echo home_url('/'); ?>bath/ganbanyoku">岩陶温熱 崋蒸洞</a></li>
            <li><a href="<?php echo home_url('/'); ?>facility/hogushidoko/">ボディーケアほぐし処</a></li>
            <li><a href="<?php echo home_url('/'); ?>facility/akasuri/">本場韓国式アカスリ</a></li>
            <li><a href="<?php echo home_url('/'); ?>facility/bali/">バリ式エステ</a></li>
            <li><a href="<?php echo home_url('/'); ?>price/">ご利用料金について</a></li>
            <li><a href="<?php echo home_url('/'); ?>access/">アクセス</a></li>
            <li><a href="<?php echo home_url('/'); ?>access#section02">近隣の見どころ</a></li>
            <li><a href="<?php echo home_url('/'); ?>faq/">よくある質問</a></li>
            <li><a href="<?php echo home_url('/'); ?>news/">華の湯だより</a></li>
          </ul>
          <p class="note">※ご注意：泥酔者、暴力団関係者、刺青・タトゥー（シール含む）のある方、他に伝染する疾患などをお持ちのお客様のご入館は固くお断りいたします。</p>
          <p class="shopinfo_pc pc">スーパー銭湯 崋の湯　　〒286-0048　千葉県成田市公津の杜2-40-1　TEL. 0476-28-2444　年中無休・駐車場完備　営業時間 10:00～25:00（最終受付時間 24:00迄）</p>
        </div><!--/.unit-->

        <div class="shopinfo_sp rsp">
          <p>スーパー銭湯 崋の湯</p>
          <p>〒286-0048　千葉県成田市公津の杜2-40-1</p>
          <p>TEL. 0476-28-2444　年中無休・駐車場完備</p>
          <p>営業時間 10:00～25:00（最終受付時間 24:00迄）</p>
        </div><!--shop_info-->
      </div><!--/.inner-->
      <p class="copyright">Copyright © 2011-2016 HANA NO YU. All Rights Reserved. 運営会社 : (有)ヒロシ</p>
    </div><!--/#footer-->
  </footer>

  <!-- script -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo home_url('/'); ?>common/js/rollover/rollover.js"></script>
  <script type="text/javascript" src="<?php echo home_url('/'); ?>common/js/rollover/opacity-rollover2.1.js"></script>
  <script type="text/javascript" src="<?php echo home_url('/'); ?>common/js/smoothScrollEx.js"></script>
<?php if(is_home()): ?>
  <script type="text/javascript" src="<?php echo home_url('/'); ?>common/js/jquery.bxslider.min.js"></script>
<?php endif; ?>

  <!--ロールオーバーの記述-->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.ophv').opOver(1.0, 0.6, 200, 200);
    });
  </script>

  <!--レスポンシブメニューボタンの記述-->
  <script type="text/javascript">
    $(document).ready(function(){
      $(window).resize(function() {
          var w = $(window).width();
          if(750 <= w){ //ここにブレイクポイントの数値を入れて条件分岐
            $('#menu').css("display","none");
          }
      });
      $('#menu_btn').on('click',function(){
        $('#menu').slideToggle();
      });
    });
  </script>

<?php if(is_home()): ?>
  <!--スライダーの記述(pc用)-->
  <script type="text/javascript">
    $(function(){
      var slide = $('#slider ul').bxSlider({
        slideWidth  : 1100,
        slideMargin : 0,
        controls    : true,
        pager       : false,
        auto        : true,
        minSlides   : 3,
        maxSlides   : 3,
        moveSlides  : 1,
        speed       : 1000,
        pause       : 5000,
        onSlideAfter: function(){
            slide.startAuto();
        }
      });
    });
  </script>

  <!--スライダーの記述（rsp用）-->
  <script type="text/javascript">
    $(function(){
      var slide = $('#slider_rsp ul').bxSlider({
        slideMargin : 0,
        controls    : true,
        pager       : false,
        auto        : true,
        speed       : 1000,
        pause       : 5000,
      });
    });
  </script>

  <!-- トップページクーポンの記述 -->
  <script type="text/javascript">
    $(function(){
      var box    = $(".coupon");
      var boxTop = box.offset().top;
      $(window).scroll(function () {
        if($(window).scrollTop() >= boxTop - 200) {
          box.addClass("fixed");
        } else {
          box.removeClass("fixed");
        }
      });
    });
  </script>

  <!-- トップページLINEの記述 -->
  <script type="text/javascript">
    $(function(){
      var box    = $(".line");
      var boxTop = box.offset().top;
      $(window).scroll(function () {
        if($(window).scrollTop() >= boxTop - 465) {
          box.addClass("fixed");
        } else {
          box.removeClass("fixed");
        }
      });
    });
  </script>
<?php endif; ?>

<?php wp_footer(); ?>
</body>
</html>