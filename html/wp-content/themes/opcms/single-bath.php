<?php $this_pagetitle = single_post_title('', false); ?>
<?php get_header(); ?>

  <div id="wrapper">
    <div id="wrapper-in">

      <nav>
        <ul id="bcarea" class="pc">
          <li><a href="<?php echo home_url('/'); ?>">top</a>&nbsp;&gt;&nbsp;</li><!--
          --><li><a href="<?php echo home_url('/'); ?>bath">お風呂のご案内</a>&nbsp;&gt;&nbsp;</li><!--
          --><li><?php echo $this_pagetitle; ?></li>
        </ul>
      </nav>

      <main>
        <div id="main" class="bath_btm">
          <div id="lnav" class="pc">
            <nav>
              <ul>
                <li><a href="<?php echo home_url('/'); ?>wanoyu/">和の湯</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>yonoyu/">洋の湯</a></li>
              </ul>
            </nav>
          </div><!-- /#lnav -->

<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php remove_filter ('acf_the_content', 'wpautop'); ?>
<?php the_field('bath_main'); ?>
<?php endwhile; endif; ?>

         </div><!-- /#main -->
      </main>

<?php get_sidebar(); ?>

    </div><!-- /#wrapper-in -->
  </div><!-- /#wrapper -->

<?php get_footer(); ?>