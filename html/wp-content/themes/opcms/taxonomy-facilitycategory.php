<?php $this_pagetitle = get_term_by('slug',$term,$taxonomy)->name; ?>
<?php
  $term_slug = get_query_var('productscategory');
  $term_info = get_term_by('slug',$term_slug,'productscategory');
  $this_term_name = $term_info->name;
  $this_term_slug = $term_info->slug;
  $this_term_id = $term_info->term_id;
?>
<?php get_header(); ?>

 <div id="wrapper">
    <div id="wrapper-in">

      <nav>
        <ul id="bcarea" class="pc">
          <li><a href="<?php echo home_url('/'); ?>">top</a>&nbsp;&gt;&nbsp;</li><!--
          --><li><a href="<?php echo home_url('/'); ?>bath">施設のご案内</a>&nbsp;&gt;&nbsp;</li><!--
          --><li><?php echo $this_pagetitle; ?></li>
        </ul>
      </nav>

<?php if (is_object_in_term($post->ID, 'facilitycategory','hogushidoko')): //ボディーケアほぐし処ページの記述 ?>
      <main>
        <div id="main" class="hogushi low-fac">
          <div id="lnav" class="pc">
            <nav>
              <ul>
                <li><a href="<?php echo home_url('/'); ?>facility/hogushidoko/">ボディーケアほぐし処</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/akasuri/">本場韓国式アカスリ･エステ</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/bali/">バリ式エステ</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/ganbanyoku/">岩陶温熱 崋蒸洞</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/restaurant/">お食事処 崋</a></li>
              </ul>
            </nav>
          </div><!-- /#lnav -->

          <section>
            <div id="section01" class="section">
              <h3 class="h3ttl"><span>ボディーケアほぐし処</span></h3>
              <div class="exparea">
                <p class="img"><img src="<?php echo home_url('/'); ?>common/img/hogushidoko/img01.jpg" alt="ほぐし処の写真" width="299" height="210"></p>
                <div class="txt">
                  <p>熟練の専任スタッフが指腹、掌を用い、コリの溜まりやすい肩のこりや首、腰など、体の疲れを深部からしっかりともみほぐしていきます。<br>湯上りの温まった体に、頭の先からつま先まで、お客様のお好みや体調に合わせてもみほぐします。</p>
                </div><!-- /.txt -->
              </div><!-- /.exparea -->

              <div class="menu-tbl-wrap">
<?php
  $args = array(
    'post_type' => 'facilitymp',
    'taxonomy' => 'facilitympcategory',
    'term' => 'bodycare',
    'posts_per_page' => 1
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
                <?php the_field('facility_menuprice'); ?>
<?php
  endforeach;
  wp_reset_postdata();
?>
              </div><!-- /.menu-tbl-wrap -->
            </div><!-- /#section01.section -->
          </section>

          <section>
            <div id="section02" class="section">
              <h3 class="h3ttl"><span>キャンペーンのお知らせ</span></h3>
              <div id="fac-news">
<?php
  $args = array(
    'post_type' => 'facility',
    'taxonomy' => 'facilitycategory',
    'term' => 'hogushidoko',
    'posts_per_page' => 1
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
                <article>
                  <div class="box">
                    <h4><?php the_title(); ?></h4>
                    <div class="entry-body">
                      <?php the_field('facility_campaign'); ?>
                    </div><!-- /.entry-body -->
                  </div><!-- /.box -->
                </article>

<?php
  endforeach;
  wp_reset_postdata();
?>
              </div><!-- /#fac-news -->
            </div><!-- /#section01.section -->
          </section>
        </div><!-- /#main -->
      </main>

<?php elseif (is_object_in_term($post->ID, 'facilitycategory','akasuri')): //アカスリページの記述 ?>
      <main>
        <div id="main" class="akasuri low-fac">
          <div id="lnav" class="pc">
            <nav>
              <ul>
                <li><a href="<?php echo home_url('/'); ?>facility/hogushidoko/">ボディーケアほぐし処</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/akasuri/">本場韓国式アカスリ･エステ</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/bali/">バリ式エステ</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/ganbanyoku/">岩陶温熱 崋蒸洞</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/restaurant/">お食事処 崋</a></li>
              </ul>
            </nav>
          </div><!-- /#lnav -->

          <section>
            <div id="section01" class="section">
              <h3 class="h3ttl"><span>本場韓国式アカスリ･エステ</span></h3>
              <div class="exparea">
                <p class="img"><img src="<?php echo home_url('/'); ?>common/img/akasuri/img01.jpg" alt="本場韓国式アカスリ･エステの写真" width="299" height="210"></p>
                <div class="txt">
                  <p>本場韓国式アカスリは、お風呂やサウナで十分にからだを温めていただき毛穴が開いた状態でおこなうので、体の古い角質や毛穴に詰まった老廃物をきれいに取り除き、お肌をスベスベにするとともに、新陳代謝を高め、血行の促進を促します。また全身オイルトリートメントや塩もみ等と一緒に行いますと、日常のストレスや疲れが癒されリラクゼーション効果が得られます。是非お風呂に入った際にお試しください。<br>和の湯、洋の湯共にございます。</p>
                </div><!-- /.txt -->
              </div><!-- /.exparea -->

              <div class="menu-tbl-wrap mb">
<?php
  $args = array(
    'post_type' => 'facilitymp',
    'taxonomy' => 'facilitympcategory',
    'term' => 'akasuri',
    'posts_per_page' => 2
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
                <?php the_field('facility_menuprice'); ?>
<?php
  endforeach;
  wp_reset_postdata();
?>
              </div><!-- /.menu-tbl-wrap mb -->
            </div><!-- /#section01.section -->
          </section>

          <section>
            <div id="section02" class="section">
              <h3 class="h3ttl"><span>キャンペーンのお知らせ</span></h3>
              <div id="fac-news">
<?php
  $args = array(
    'post_type' => 'facility',
    'taxonomy' => 'facilitycategory',
    'term' => 'akasuri',
    'posts_per_page' => 1
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
                <article>
                  <div class="box">
                    <h4><?php the_title(); ?></h4>
                    <div class="entry-body">
<?php the_field('facility_campaign') ?>
                    </div><!-- /.entry-body -->
                  </div><!-- /.box -->
                </article>
<?php
  endforeach;
  wp_reset_postdata();
?>
              </div><!-- /#fac-news -->
            </div><!-- /#section01.section -->
          </section>
         </div><!-- /#main -->
      </main>

<?php elseif (is_object_in_term($post->ID, 'facilitycategory','bali')): //バリ式エステページの記述 ?>
      <main>
        <div id="main" class="bali low-fac">
          <div id="lnav" class="pc">
            <nav>
              <ul>
                <li><a href="<?php echo home_url('/'); ?>facility/hogushidoko/">ボディーケアほぐし処</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/akasuri/">本場韓国式アカスリ･エステ</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/bali/">バリ式エステ</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/ganbanyoku/">岩陶温熱 崋蒸洞</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/restaurant/">お食事処 崋</a></li>
              </ul>
            </nav>
          </div><!-- /#lnav -->

          <section>
            <div id="section01" class="section">
              <h3 class="h3ttl"><span>バリ式エステ</span></h3>
              <div class="exparea">
                <p class="img"><img src="<?php echo home_url('/'); ?>common/img/bali/img01.jpg" alt="バリ式エステの写真" width="299" height="210"></p>
                <div class="txt">
                  <p>気軽にほぐせるクイック＆本格的バリ式エステです。 <br>全身のリンパを流し老廃物を排出し、体の中からキレイになり癒されます。 </p>
                </div><!-- /.txt -->
              </div><!-- /.exparea -->

              <div class="menu-tbl-wrap">
<?php
  $args = array(
    'post_type' => 'facilitymp',
    'taxonomy' => 'facilitympcategory',
    'term' => 'bali',
    'posts_per_page' => 1
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
                <?php the_field('facility_menuprice') ?>
<?php
  endforeach;
  wp_reset_postdata();
?>
              </div><!-- /menu-tbl-wrap -->
            </div><!-- /#section01.section -->
          </section>

          <section>
            <div id="section02" class="section">
              <h3 class="h3ttl"><span>キャンペーンのお知らせ</span></h3>
              <div id="fac-news">
<?php
  $args = array(
    'post_type' => 'facility',
    'taxonomy' => 'facilitycategory',
    'term' => 'bali',
    'posts_per_page' => 1
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
                <article>
                  <div class="box">
                    <h4><?php the_title(); ?></h4>
                    <div class="entry-body">
                      <?php the_field('facility_campaign') ?>
                    </div><!-- /.entry-body -->
                  </div><!-- /.box -->
                </article>
<?php
  endforeach;
  wp_reset_postdata();
?>
              </div><!-- /#fac-news -->
            </div><!-- /#section01.section -->
          </section>
        </div><!-- /#main -->
      </main>

<?php elseif (is_object_in_term($post->ID, 'facilitycategory','ganbanyoku')): // 岩陶温熱 崋蒸洞ページの記述 ?>
      <main>
        <div id="main" class="ganbanyoku low-fac">
          <div id="lnav" class="pc">
            <nav>
              <ul>
                <li><a href="<?php echo home_url('/'); ?>facility/hogushidoko/">ボディーケアほぐし処</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/akasuri/">本場韓国式アカスリ･エステ</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/bali/">バリ式エステ</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/ganbanyoku/">岩陶温熱 崋蒸洞</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/restaurant/">お食事処 崋</a></li>
              </ul>
            </nav>
          </div><!-- /#lnav -->

          <section>
            <div id="section01" class="section">
              <h3 class="h3ttl"><span>岩陶温熱 崋蒸洞</span></h3>
              <div class="exparea">
                <p class="img"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/img01.jpg" alt="崋蒸洞の写真" width="299" height="210"></p>
                <div class="txt">
                  <p>赤外線効果のある「セラミックボール」や岩塩を用いた湯を使わない足湯と、ゲルマ鉱石や水晶など他種類の薬宝石を使用した岩盤浴です。<br>二部屋の温熱浴を５０分間のプログラムでご自由にお楽しみ頂ける崋の湯独自の温熱浴です。</p>
                </div><!-- /.txt -->
              </div><!-- /.exparea -->
              <div class="menu-tbl-wrap">
<?php
  $args = array(
    'post_type' => 'facilitymp',
    'taxonomy' => 'facilitympcategory',
    'term' => 'ganbanyoku',
    'posts_per_page' => 1
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
                <?php the_field('facility_menuprice') ?>
<?php
  endforeach;
  wp_reset_postdata();
?>
              </div><!-- /.menu-tbl-wrap -->

              <ul>
                <li>※盤欲は大人（中学生以上）のご利用とさせて頂いております。</li>
                <li>※岩盤浴料には専用着・マット含みます。</li>
              </ul>
              <p class="sbttl">温熱浴の効果的利用合法</p>
              <p class="sbhead">プログラム開始時間　AM11：30～PM22：30<br class="rsp"> （毎時 30分より開始）</p>
              <ul>
                <li>※ダブル（２回）ご利用がオススメです。「ダブルご利用」とは岩盤浴を２回連続してお入り頂きます。ダイエット・デトックス（解毒）効果を高めるのに効果的です。途中５分程度の休憩、水分補給をするのがベストです。</li>
                <li>※水分をたっぷり補給しましょう。500ml程度の水分補給（ペットボトル約１本）が新陳代謝を活発にしてくれます。崋蒸洞ではミネラルたっぷりの「生効水」をご用意しております。</li>
                </ul>
            </div><!-- /#section01.section -->
          </section>

          <section>
            <div id="section02" class="section">
              <h3 class="h3ttl"><span>ご利用の流れ</span></h3>
              <p class="flow-img pc"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/flow.gif" alt="ご利用の流れ図" width="745" height="131"></p>

              <ul id="flow-list">
                <li>
                  <div class="box">
                    <p class="ttl"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/flow_num01.gif" alt="1" width="47" height="47">受付</p>
                    <div class="txt">
                      <p>券売機にてご利用券をお買い求めになり、1Ｆ受付にてご予約して専用着をお受取ください。</p>
                    </div><!-- /.txt -->
                  </div><!-- /.box -->
                </li>

                <li>
                  <div class="box">
                    <p class="ttl"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/flow_num02.gif" alt="2" width="47" height="47">入浴</p>
                    <div class="txt">
                      <p>まずはお風呂にて、ごゆっくりとお身体を温めてください。</p>
                    </div><!-- /.txt -->
                  </div><!-- /.box -->
                </li>

                <li>
                  <div class="box">
                    <p class="ttl"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/flow_num03.gif" alt="3" width="47" height="47">着替え・洗顔</p>
                    <div class="txt">
                      <p>ご予約時に受け取った専用着に着替えていただき、ご予約の５分前に崋蒸洞受付にてマットをお受取ください。</p>
                    </div><!-- /.txt -->
                  </div><!-- /.box -->
                </li>

                <li>
                  <div class="box">
                    <p class="ttl"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/flow_num04.gif" alt="4" width="47" height="47">水分補給</p>
                    <div class="txt">
                      <p>十分水分を補給して下さい。発汗効果を高めます。</p>
                    </div><!-- /.txt -->
                  </div><!-- /.box -->
                </li>

                <li>
                  <div class="box">
                    <p class="ttl"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/flow_num05.gif" alt="5" width="47" height="47">足・膝の温め</p>
                    <div class="txt">
                      <p>汗効効果を高めます。 陶板温熱足の癒で、脚・腰を温めてください。</p>
                    </div><!-- /.txt -->
                  </div><!-- /.box -->
                </li>

                <li>
                  <div class="box">
                    <p class="ttl"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/flow_num06.gif" alt="6" width="47" height="47">岩盤浴</p>
                    <div class="txt">
                      <p>崋の癒でマットを敷いて横になり、岩盤温熱浴でごゆっくりご静養ください。体の隅々まで、岩盤の熱が浸透していきます。次第に汗もしたたり落ち、体内の中で体を綺麗にする活動が活発になっていきます。</p>
                    </div><!-- /.txt -->
                  </div><!-- /.box -->
                </li>

                <li>
                  <div class="box">
                    <p class="ttl"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/flow_num07.gif" alt="7" width="47" height="47">休憩</p>
                    <div class="txt">
                      <p>プログラム終了後は再度水分を補給してください。休憩中も発汗が続きます。</p>
                    </div><!-- /.txt -->
                  </div><!-- /.box -->
                </li>

                <li>
                  <div class="box">
                    <p class="ttl"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/flow_num08.gif" alt="8" width="47" height="47">着替え</p>
                    <div class="txt">
                      <p>ご使用済みのマット・専用着は専用のボックスにご返却ください。</p>
                    </div><!-- /.txt -->
                  </div><!-- /.box -->
                </li>

                <li>
                  <div class="box">
                    <p class="ttl"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/flow_num09.gif" alt="9" width="47" height="47">入浴</p>
                    <div class="txt">
                      <p>お風呂にて汗をお流しください。</p>
                    </div><!-- /.txt -->
                  </div><!-- /.box -->
                </li>
              </ul><!-- /#flow-list -->
            </div><!-- /#section02.section -->
          </section>

          <section>
            <div id="section03" class="section">
              <h3 class="h3ttl"><span>陶板温熱　足の癒</span></h3>
              <div class="exparea">
                <p class="img"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/img02.jpg" alt="足の癒の写真" width="299" height="210"></p>
                <div class="txt">
                  <p>高い遠赤外線効果が期待できる陶板は、発汗作用を促し、自然治癒力を高めると言われています。<br>セラミックボールによる足底のツボへの心地よい刺激と、陶板の遠赤外線効果により下半身を温め、良好な血液循環が期待されます。</p>
                </div><!-- /.txt -->
              </div><!-- /.exparea -->
              <ul class="stones">
                <li>
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/img02-2.jpg" alt="岩塩の写真" width="150" height="110"></p>
                  <div class="txt">
                    <p class="ttl">岩塩</p>
                    <p>数億年前、地球の地殻変動で海水と階層が一緒に山脈の中に固まって、高濃度のミネラル成分が結晶化して形成されたヒマラヤ岩塩は自然に最も近いミネラル岩塩です。空気浄化に優れている岩塩です。</p>
                  </div><!-- /.txt -->
                </li>
              </ul>
            </div><!-- /#section03.section -->
          </section>

          <section>
            <div id="section04" class="section">
              <h3 class="h3ttl"><span>岩盤浴　崋の癒</span></h3>
              <div class="exparea">
                <p class="img"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/img03.jpg" alt="崋の癒の写真" width="299" height="210"></p>
                <div class="txt">
                  <p>床に敷かれた岩盤「薬宝玉石」はミネラル成分を豊富に含み、遠赤外線を放出します。着衣を身に着けたままマットを敷いて、温かな岩盤に横たわるだけで身体全体が温まり心地良く発汗作用を促します。立体音響によるヒーリング「癒し」効果も合わせてお楽しみ下さい。<br>薬宝玉石とは、古来より奇跡の健康石と呼ばれ、パワーストーンとして最も歴史のある天然石の一つです。遠赤外線を放出し、ミネラル分を多く含み、代謝機能の促進や体内への酸素補給の増加が期待できます。</p>
                </div><!-- /.txt -->
              </div><!-- /.exparea -->

              <ul class="stones">
                <li>
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/img03-2.jpg" alt="ゲルマ鉱石の写真" width="150" height="110"></p>
                  <div class="txt">
                    <p class="ttl">ゲルマ鉱石</p>
                    <p>32℃以上に温めると、マイナス電子を放出します。<br>そのマイナス電子が人体の新陳代謝を活発にし、腰痛・肩こり・冷え性・不眠・慢性疲労などの改善に一役買います。また、生体電流を調整してくれます。</p>
                  </div><!-- /.txt -->
                </li>

                <li>
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/img03-3.jpg" alt="縁色薬宝玉石の写真" width="150" height="110"></p>
                  <div class="txt">
                    <p class="ttl">縁色薬宝玉石</p>
                    <p>古来により奇跡の健康玉石と呼ばれ、いわゆるパワーストーンとして、最も歴史のある石の一つです。遠赤外線を放出し、ミネラル成分を多量に含むことから、新陳代謝の促進や体内への酸素補給の増加が期待できます。古代中国では、「肺・心臓・声帯を強化する」と言われ、珍重されてきました。</p>
                  </div><!-- /.txt -->
                </li>

                <li>
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/img03-4.jpg" alt="木紋石（きもんせき）の写真" width="150" height="110"></p>
                  <div class="txt">
                    <p class="ttl">縁色薬宝玉石</p>
                    <p>木目のような美しい模様が特徴ですが、高い遠赤外線効果とマイナスイオンを放出することから、脱臭や殺菌効果に優れます。また、細胞を活性化させ、活力を取り戻したり、体内時計を正確に戻す作用もあると言われています。</p>
                  </div><!-- /.txt -->
                </li>

                <li>
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/ganbanyoku/img03-5.jpg" alt="瑪瑙・水晶の写真" width="150" height="110"></p>
                  <div class="txt">
                    <p class="ttl">縁色薬宝玉石</p>
                    <p>古くから悪魔を祓う際に用いられてきた瑪瑙は、心の中にある負の感情を解放し、気持ちを落ち着かせる効果があります。また、混ぜ込んである水晶は、マイナスイオンを放出し、環境を浄化する力があります。不眠の穏和、解毒作用や血液の浄化作用があると言われています。</p>
                  </div><!-- /.txt -->
                </li>
              </ul>
            </div><!-- /#section01.section -->
          </section>
        </div><!-- /#main -->
      </main>

<?php elseif (is_object_in_term($post->ID, 'facilitycategory','restaurant')): // 食事処 崋ページの記述 ?>
      <main>
        <div id="main" class="restaurant low-fac">
          <div id="lnav" class="pc">
            <nav>
              <ul>
                <li><a href="<?php echo home_url('/'); ?>facility/hogushidoko/">ボディーケアほぐし処</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/akasuri/">本場韓国式アカスリ･エステ</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/bali/">バリ式エステ</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/ganbanyoku/">岩陶温熱 崋蒸洞</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li><!--
                --><li><a href="<?php echo home_url('/'); ?>facility/restaurant/">お食事処 崋</a></li>
              </ul>
            </nav>
          </div><!-- /#lnav -->

          <section>
            <div id="section01" class="section">
              <h3 class="h3ttl"><span>お食事処 崋</span></h3>
              <div class="exparea">
                <p class="img"><img src="<?php echo home_url('/'); ?>common/img/restaurant/img01.jpg" height="210" width="299" alt="お食事処 崋の写真"></p>
                <div class="txt">
                  <p>お風呂上りに美味しいお食事をお楽しみいただく為に、食事処崋では、多彩なメニューをご用意しています。<br>定食・丼物をはじめ多彩なメニューで季節にあわせて、取り揃えております。手作りたぬき豆腐は当店お勧めです！<br>
                  営業時間　AM11:00～深夜0:00（ラストオーダー　PM11:00）</p>
                </div><!-- /.txt -->
              </div><!-- /.exparea -->

              <div class="menu_list">
                <ul><!--
<?php
  $args = array(
    'post_type' => 'facility',
    'taxonomy' => 'facilitycategory',
    'term' => 'restaurant',
    'posts_per_page' => 9
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );

  $attachment_id = get_field('restaurant_img');
  $size = "cs_restaurant_img"; // (thumbnail, medium, large, full or custom size)
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('restaurant_img') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
                  --><li>
                    <p><img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>の写真" /></p>
                    <p class="txt02"><?php the_title(); ?></p>
                  </li><!--

<?php
  endforeach;
  wp_reset_postdata();
?>
                --></ul>
              </div><!--/.menu_list-->
            </div><!-- /#section01.section -->
          </section>
        </div><!-- /#main -->
      </main>
<?php endif; ?>

<?php get_sidebar(); ?>

    </div><!-- /#wrapper-in -->
  </div><!-- /#wrapper -->

<?php get_footer(); ?>

<?php get_footer(); ?>