<?php $this_pagetitle = get_post_type_object(get_post_type())->label; ?>
<?php get_header(); ?>

  <div id="wrapper">
    <div id="wrapper-in">

      <nav>
        <ul id="bcarea" class="pc"><!--
          --><li><a href="<?php echo home_url('/'); ?>">top</a>&nbsp;&gt;&nbsp;</li><!--
          --><li><?php echo $this_pagetitle; ?></li><!--
        --></ul>
      </nav>

      <main>
        <div id="main" class="facility">
          <div class="top-txt">
            <p>千葉県成田市にある崋の湯では、ご来館のお客様に、一日ゆっくり過ごしていただくために、お風呂施設のほか岩盤浴やサウナ、エステ、マッサージ、お食事も楽しんで頂けるコーナーやリラクゼーション施設も充実しております。</p>
            <p>お風呂やサウナでゆっくりと汗を流した後は、マッサージやエステ、無料の休憩所などで心ゆくまでお寛ぎください。</p>
          </div><!-- /.top-txt -->

          <section>
            <div id="section01" class="section">
              <h3 class="h3ttl"><span>施設のご紹介</span></h3>
              <ul id="fac-list">
                <li>
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/facility/fac_img01.gif" alt="崋の湯の写真" width="299" height="210"></p>
                  <div class="txtarea">
                    <p class="ttl">癒しの湯 崋の湯</p>
                    <div class="txt">
                      <p>お風呂は全て軟水を使用。趣の異なる和洋２種類の全２４種類の浴場が、日替わりで男女入れ替え制になっています。<br>緑豊かな露天風呂の高濃度人工炭酸泉で肌はつるつる。サウナで気持ちの良い汗をかいたり、季節に応じた替わり湯が楽しめる「温泉めぐりの湯」などのんびりと湯巡りを楽しむことが出来ます。</p>
                    </div><!-- /.txt -->
                    <p class="link"><a href="<?php echo home_url('/'); ?>bath/">●詳細はこちら「崋の湯」</a></p>
                  </div><!-- /.txtarea -->
                </li>

                <li>
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/facility/fac_img02.gif" alt="岩盤浴 華蒸洞の写真" width="299" height="210"></p>
                  <div class="txtarea">
                    <p class="ttl">岩盤浴 華蒸洞</p>
                    <div class="txt">
                      <p>赤外線効果のある「セラミックボール」や岩塩を用いた湯を使わない足湯と、ゲルマ鉱石や水晶など他種類の薬宝石を使用した岩盤浴です。<br>二部屋の温熱浴を５０分間のプログラムでご自由にお楽しみ頂ける崋の湯独自の温熱浴です。</p>
                    </div><!-- /.txt -->
                    <p class="link"><a href="<?php echo home_url('/'); ?>/facility/ganbanyoku/">●詳細はこちら「華蒸洞」</a></p>
                  </div><!-- /.txtarea -->
                </li>

                <li>
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/facility/fac_img03.gif" alt="ボディーケアほぐし処の写真" width="299" height="210"></p>
                  <div class="txtarea">
                    <p class="ttl">ボディーケアほぐし処</p>
                    <div class="txt">
                      <p>熟練の専任スタッフが指腹、掌を用い、コリの溜まりやすい肩のこりや首、腰など、体の疲れを深部からしっかりともみほぐしていきます。<br>湯上りの温まった体に、頭の先からつま先まで、お客様のお好みや体調に合わせてもみほぐします。</p>
                    </div><!-- /.txt -->
                    <p class="link"><a href="<?php echo home_url('/'); ?>facility/hogushidoko/">●詳細はこちら「ボディーケアほぐし処」</a></p>
                  </div><!-- /.txtarea -->
                </li>


                <li>
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/facility/fac_img04.gif" alt="本場韓国式アカスリ・エステの写真" width="299" height="210"></p>
                  <div class="txtarea">
                    <p class="ttl">本場韓国式アカスリ・エステ</p>
                    <div class="txt">
                      <p>本場韓国式アカスリは、お風呂やサウナで十分にからだを温めて毛穴が開いた状態でおこないます。体の古い角質や老廃物を取り除くとともに、新陳代謝を高め、血行の促進を促します。また全身オイルトリートメントや塩もみ等と一緒に行いますと、日常のストレスや疲れが癒されリラクゼーション効果が得られます。</p>
                    </div><!-- /.txt -->
                    <p class="link"><a href="<?php echo home_url('/'); ?>facility/akasuri/">●詳細はこちら「本場韓国式アカスリ・エステ」</a></p>
                  </div><!-- /.txtarea -->
                </li>

                <li>
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/facility/fac_img05.gif" alt="バリ式エステの写真" width="299" height="210"></p>
                  <div class="txtarea">
                    <p class="ttl">バリ式エステ</p>
                    <div class="txt">
                      <p>気軽にほぐせるクイック＆本格的バリ式エステです。 <br>全身のリンパを流し老廃物を排出し、体の中からキレイになり癒されます。 </p>
                    </div><!-- /.txt -->
                    <p class="link"><a href="<?php echo home_url('/'); ?>facility/bali/">●詳細はこちら「バリ式エステ」</a></p>
                  </div><!-- /.txtarea -->
                </li>


                <li>
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/facility/fac_img06.gif" alt="ゆっくり休憩寝転座敷の写真" width="299" height="210"></p>
                  <div class="txtarea">
                    <p class="ttl">ゆっくり休憩寝転座敷</p>
                    <div class="txt">
                      <p>お風呂上りに、温まった体を休めていただく為に、男女共有のお休み処をご用意しています。<br>日本ならではの座敷でゆっくりと、温まった体をお休めください。</p>
                    </div><!-- /.txt -->
                  </div><!-- /.txtarea -->
                </li>


                <li id="restaurant">
                  <p class="img"><img src="<?php echo home_url('/'); ?>common/img/facility/fac_img07.gif" alt="食事処 崋の写真" width="299" height="210"></p>
                  <div class="txtarea">
                    <p class="ttl">食事処 崋</p>
                    <div class="txt">
                      <p>お風呂上りに美味しいお食事をお楽しみいただく為に、食事処崋では、多彩なメニューをご用意しています。<br>定食・丼物をはじめ多彩なメニューで季節にあわせて、取り揃えております。手作りたぬき豆腐は当店お勧めです！</p>
                    </div><!-- /.txt -->
                    <p class="link"><a href="<?php echo home_url('/'); ?>facility/restaurant/">●食事処 崋</a></p>
                  </div><!-- /.txtarea -->
                </li>
              </ul>
            </div><!-- /#section01.section -->
          </section>
        </div><!-- /#main -->
      </main>

<?php get_sidebar(); ?>

    </div><!-- /#wrapper-in -->
  </div><!-- /#wrapper -->

<?php get_footer(); ?>