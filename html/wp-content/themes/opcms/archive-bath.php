<?php $this_pagetitle = get_post_type_object(get_post_type())->label; ?>
<?php get_header(); ?>

  <div id="wrapper">
    <div id="wrapper-in">

      <nav>
        <ul id="bcarea" class="pc"><!--
          --><li><a href="<?php echo home_url('/'); ?>">top</a>&nbsp;&gt;&nbsp;</li><!--
          --><li><?php echo $this_pagetitle; ?></li><!--
        --></ul>
      </nav>

      <main>
        <div id="main" class="bath">
          <section>
            <div id="section01">
              <p class="img_map rsp"><img src="<?php echo home_url('/'); ?>common/img/bath/sec01_bg.jpg" height="366" width="745" alt="案内図"></p>
              <h3>男女日替わりでお楽しみください</h3>
              <div class="txt_area01">
                <p>崋の湯では「和の湯」と「洋の湯」に分かれております。</p>
                <p class="txt">「和の湯」と「洋の湯」合わせて、２４種類のお風呂をご用意しています。男女入れ替え制の日替わり湯をお楽しみいただくことができます。たっぷりの湯舟に贅沢につかる　癒しの湯をご堪能ください。入れ替えの予定につきましては下記となります。</p>
                <p class="note">●&nbsp;&nbsp;「和の湯」は、偶数日が男湯、奇数日が女湯</p>
                <p class="note">●&nbsp;&nbsp;「洋の湯」は、奇数日が男湯、偶数日が女湯</p>
              </div><!--/.txt_area-->

              <div class="wanoyu">
                <a href="<?php echo home_url('/'); ?>wanoyu/" class="ophv banner_pc pc"><span>塩サウナやお灸バスなど、和風ならではのお風呂をお楽しみください。</span>
                </a>
                <a href="<?php echo home_url('/'); ?>wanoyu/" class="ophv banner_rsp rsp">
                  <img src="<?php echo home_url('/'); ?>common/img/rsp/wanoyu_banner.jpg" height="104" width="337" alt="和の湯">
                </a>
                <p class="pc">「和の湯」の詳細はこちらからご確認ください</p>
              </div><!--/.wanoyu-->
              <div class="yonoyu">
                <a href="<?php echo home_url('/'); ?>yonoyu/" class="ophv banner_pc pc"><span>ミストサウナやパワーバスなど洋風ならではのお風呂をお楽しみください。</span>
                </a>
                <a href="<?php echo home_url('/'); ?>yonoyu/" class="ophv banner_rsp rsp">
                  <img src="<?php echo home_url('/'); ?>common/img/rsp/yonoyu_banner.jpg" height="102" width="338" alt="洋の湯" class="rsp">
                </a>
                <p class="pc">「洋の湯」の詳細はこちらからご確認ください</p>
              </div><!--/.wanoyu-->

              <section>
                <h4>軟水泉使用</h4>
                <div class="unit">
                  <div class="txt_area02">
                    <p class="txt01">崋の湯では、軟水泉を使用しております。軟水は汚れを完全に落とし、皮ふの新陳代謝を活発にし、血行を促進します。また、美容と健康上の効用から水の宝石や「美人の湯」とも言われます。</p>
                    <h5>軟水の素晴らしさ</h5>
                    <p>●&nbsp;&nbsp;肌の保湿効果が増し、温泉のようなスベスベ感を実感！</p>
                    <p>●&nbsp;&nbsp;湯上がりの温かさが長持ち！</p>
                    <p>●&nbsp;&nbsp;汚れにくく、いつも清潔！</p>
                    <p>●&nbsp;&nbsp;毛髪もしっとり傷みにくい！</p>
                  </div><!--/.txt_area02-->
                  <p class="img pc"><img src="<?php echo home_url('/'); ?>common/img/bath/nansui_img.jpg" height="199" width="300" alt="軟水泉使用"></p>
                </div><!--/.unit-->
              </section>
            </div><!--/#section01-->
          </section>

          <section>
            <div id="section04" class="section">
              <h3>変わり湯スケジュール</h3>
              <section>
                <div class="unit">
                  <h4>露天風呂　変わり湯</h4>

<?php
  $args = array(
    'post_type' => 'bath',
    'taxonomy' => 'bathcategory',
    'term' => 'out',
    'posts_per_page' => 1,
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
                    <?php the_field('bath_text'); ?>
<?php
  endforeach;
  wp_reset_postdata();
?>

                </div><!--/.unit-->
              </section>

              <section>
                <div class="unit">
                  <h4>内風呂　変わり湯</h4>

<?php
  $args = array(
    'post_type' => 'bath',
    'taxonomy' => 'bathcategory',
    'term' => 'in',
    'posts_per_page' => 1,
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
                    <?php the_field('bath_text'); ?>
<?php
  endforeach;
  wp_reset_postdata();
?>

                </div><!--/.unit-->
              </section>
            </div><!--/#section04-->
          </section>

          <section>
            <div id="section02" class="section">
              <h3>安全宣言</h3>
              <div class="txt_area">
                <p>崋の湯では、日々衛生管理に最善を尽くし、公的機関による水質検査を実施しております。皆様にご安心してご利用いただけますよう、ここに安全宣言をいたします。</p>
                <p>なお、当店をご利用のお客様にも下記事項のご協力をお願い申し上げます。</p>
              </div><!--/.txt_area-->
              <p>●&nbsp;&nbsp;湯船に入る前に必ず「かけ湯」をする。</p>
              <p>●&nbsp;&nbsp;湯船には潜らない。</p>
              <p>●&nbsp;&nbsp;タオルは湯船に入れない。</p>
              <p>●&nbsp;&nbsp;浴槽水は絶対に飲まない</p>
            </div><!--/#section02-->
          </section>

          <section>
            <div id="section03" class="section">
              <h3>ご利用上のお願い</h3>
              <p class="txt">●&nbsp;&nbsp;当浴場は銭湯となっております。浴場には、シャンプーやボディソープを備え付けておりますが、タオルやその他の入浴用品は、お客様でご持参いただくか、館内にてご購入下さい。 </p>
              <p class="txt">●&nbsp;&nbsp;泥酔・刺青・タトゥー（シール含む）の方、暴力団並びにその関係者と見なされる方はご入館を固くお断りいたします。</p>
              <p class="txt">●&nbsp;&nbsp;伝染する疾患などをお持ちのお客様の入浴はご遠慮ください。</p>
              <p class="txt">●&nbsp;&nbsp;館内への飲食物の持ち込みは保健所の指導により固くお断りします。</p>
              <p class="txt">●&nbsp;&nbsp;館内へのペットのお持ち込みはご遠慮ください。</p>
              <p class="txt">●&nbsp;&nbsp;小学校2年生以上の方の男女混浴はお断りいたします。</p>
              <p class="txt">●&nbsp;&nbsp;館内での怪我、事故、盗難にはお客様各自で充分にご注意ください。なお事故盗難などの責任は当館では負いかねますのでご了承ください。</p>
            </div><!--/#section03-->
          </section>
        </div><!-- /#main -->
      </main>

<?php get_sidebar(); ?>

    </div><!-- /#wrapper-in -->
  </div><!-- /#wrapper -->

<?php get_footer(); ?>