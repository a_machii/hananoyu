<?php $this_pagetitle = get_post_type_object(get_post_type())->label; ?>
<?php get_header(); ?>

  <div id="wrapper">
    <div id="wrapper-in">

      <nav>
        <ul id="bcarea" class="pc"><!--
          --><li><a href="<?php echo home_url('/'); ?>">top</a>&nbsp;&gt;&nbsp;</li><!--
          --><li><?php echo $this_pagetitle; ?></li><!--
        --></ul>
      </nav>

      <main>
        <div id="main" class="news">
<?php
  //$paged = get_query_var('paged');
  $args = array(
    'post_type' => array('news','facility','bath'),
    'tax_query' => array(
      'relation' => 'OR',
      array(
        'taxonomy' => 'newscategory',
        'field' => 'slug',
        'terms' => array('new','daily')
      ),
      array(
        'taxonomy' => 'facilitycategory',
        'field' => 'slug',
        'terms' => array('bali','hogushidoko','akasuri')
      ),
      array(
        'taxonomy' => 'bathcategory',
        'field' => 'slug',
        'terms' => array('in','out')
      )
    ),
    'posts_per_page' => 10,
    'paged'=>$paged
  );

  $the_query = new WP_Query($args);
?>

<?php if($the_query->have_posts()): while($the_query->have_posts()): $the_query->the_post(); //ループここから ?>
          <article>
<?php //post_type news/facilityの出力ここから
  if(get_post_type() == 'news' || get_post_type() == 'facility'):
    $terms = get_the_terms( $post->ID, array('newscategory','facilitycategory') );
      foreach($terms as $term){
        $term_name = $term->name;
        $term_slug = $term->slug;
      }
?>
            <p class="tag">
              <img src="<?php echo home_url('/'); ?>common/img/news/tag_<?php echo $term_slug; ?>.jpg" alt="<?php echo $term_name; ?>" width="141" height="112">
            </p>
            <div class="entry-box">
              <div class="txtarea">
                <p class="date"><?php the_time('Y.n.j') ?></p>
                <h3 id="entry<?php the_ID(); ?>"><?php the_title(); ?></h3>
                <div class="entry-body">
<?php $article_id = get_field('news_article01') ?>
<?php if(!empty($article_id)): ?>
                  <p><?php the_field('news_article01'); ?></p>
<?php else: ?>
                  <?php the_field('facility_campaign'); ?>
<?php endif; ?>
                </div><!-- /.entry-body -->
              </div><!-- /.txtarea -->
            </div><!-- /.entry-box -->

<?php //post_type bathの出力ここから
  elseif(get_post_type() == 'bath'):
    $terms = get_the_terms( $post->ID, array('bathcategory') );
      foreach($terms as $term){
        $term_name = $term->name;
        $term_slug = $term->slug;
      }
?>
            <p class="tag">
             <img src="<?php echo home_url('/'); ?>common/img/news/tag_daily.jpg" alt="日替わり湯スケジュール案内" width="141" height="112">
            </p>
            <div class="entry-box">
              <div class="txtarea">
                <p class="date"><?php the_time('Y.n.j') ?></p>
                <h3 id="entry<?php the_ID(); ?>"><?php the_title(); ?></h3>
                <div class="entry-body">
                  <?php the_field('bath_text'); ?>
                </div><!-- /.entry-body -->
              </div><!-- /.txtarea -->
            </div><!-- /.entry-box -->
<?php endif; //post_type news/facility/bathの出力ここまで ?>
          </article>

<?php endwhile; endif; //ループここまで ?>
<?php wp_reset_postdata(); ?>

<?php //ページネーション
  if (function_exists("pagination")) {
    pagination($additional_loop->max_num_pages);
  }
?>
        </div><!-- /#main -->
      </main>

<?php get_sidebar(); ?>

    </div><!-- /#wrapper-in -->
  </div><!-- /#wrapper -->

<?php get_footer(); ?>