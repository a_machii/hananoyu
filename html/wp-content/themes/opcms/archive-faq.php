<?php $this_pagetitle = get_post_type_object(get_post_type())->label; ?>
<?php get_header(); ?>

  <div id="wrapper">
    <div id="wrapper-in">

      <nav>
        <ul id="bcarea" class="pc"><!--
          --><li><a href="<?php echo home_url('/'); ?>">top</a>&nbsp;&gt;&nbsp;</li><!--
          --><li><?php echo $this_pagetitle; ?></li><!--
        --></ul>
      </nav>

      <main>
        <div id="main" class="faq">
          <div id="section01" class="pc">
            <p>施設および入浴に関して、よくお問い合わせいただく内容を記載いたしました。こちらに掲載されていない情報はお電話にてお気軽にお問い合わせくださいませ。</p>
            <div id="lnav" class="pc">
              <nav>
                <ul>
                  <li><a href="#section02">営業に関するご質問</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</li>
                  <li><a href="#section03">入浴に関するご質問</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</li>
                  <li><a href="#section04">施設に関するご質問</a></li>
                </ul>
              </nav>
            </div><!-- /#lnav -->
          </div><!--#section01-->

          <section>
            <div id="section02" class="section">
              <h3>営業に関するご質問</h3>
<?php
  $args = array(
    'post_type' => 'faq',
    'taxonomy' => 'faqcategory',
    'term' => 'eigyo',
    'posts_per_page' => -1
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
              <section>
                <div class="unit">
                  <h4>Q. <?php the_title(); ?></h4>
                  <div class="txt_area">
                    <?php the_field('faq_text'); ?>
                  </div><!--/.txt_area-->
                </div><!--/.unit-->
              </section>

<?php
  endforeach;
  wp_reset_postdata();
?>
            </div><!--/#section02-->
          </section>

          <section>
            <div id="section03" class="section">
              <h3>入浴に関するご質問</h3>
<?php
  $args = array(
    'post_type' => 'faq',
    'taxonomy' => 'faqcategory',
    'term' => 'nyuyoku',
    'posts_per_page' => -1
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
              <section>
                <div class="unit">
                  <h4>Q. <?php the_title(); ?></h4>
                  <div class="txt_area">
                    <?php the_field('faq_text'); ?>
                  </div><!--/.txt_area-->
                </div><!--/.unit-->
              </section>

<?php
  endforeach;
  wp_reset_postdata();
?>
            </div><!--/#section03-->
          </section>

          <section>
            <div id="section04" class="section">
              <h3>施設に関するご質問</h3>
<?php
  $args = array(
    'post_type' => 'faq',
    'taxonomy' => 'faqcategory',
    'term' => 'shisetsu',
    'posts_per_page' => -1
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
              <section>
                <div class="unit">
                  <h4>Q. <?php the_title(); ?></h4>
                  <div class="txt_area">
                    <?php the_field('faq_text'); ?>
                  </div><!--/.txt_area-->
                </div><!--/.unit-->
              </section>

<?php
  endforeach;
  wp_reset_postdata();
?>
            </div><!--/#section04-->
          </section>
        </div><!-- /#main -->
      </main>

<?php get_sidebar(); ?>

    </div><!-- /#wrapper-in -->
  </div><!-- /#wrapper -->

<?php get_footer(); ?>