<?php get_header(); ?>

  <main>
    <div class="contents" id="top">
      <div class="section" id="section01">
        <div class="inner">
          <ul id="bath_type_pc" class="pc">
            <li class="ophv"><a href="<?php echo home_url('/'); ?>bath/wanoyu/"><span>蒸し風呂やお灸バスなど、和風ならではのお風呂をお楽しみください。</span></a></li>
            <li class="ophv"><a href="<?php echo home_url('/'); ?>bath/yonoyu/"><span>ミストサウナやパワーバスなど洋風ならではのお風呂をお楽しみください。</span></a></li>
            <li class="ophv"><a href="<?php echo home_url('/'); ?>facility/ganbanyoku/"><span>ゲルマ鉱石や水晶などの薬宝玉石を使用した岩盤浴。崋の湯独自の温熱浴です。</span></a></li>
          </ul>

          <ul id="bath_type_rsp" class="rsp">
            <li><a href="<?php echo home_url('/'); ?>bath/wanoyu/" class="ophv"><img src="<?php echo home_url('/'); ?>common/img/top/wanoyu.png" height="235" width="313" alt="和の湯"></a></li>
            <li><a href="<?php echo home_url('/'); ?>bath/yonoyu/" class="ophv"><img src="<?php echo home_url('/'); ?>common/img/top/yonoyu.png" height="235" width="313" alt="洋の湯"></a></li>
            <li><a href="<?php echo home_url('/'); ?>facility/ganbanyoku/" class="ophv"><img src="<?php echo home_url('/'); ?>common/img/top/ganbanyoku.png" height="235" width="313" alt="岩盤浴"></a></li>
          </ul>
        </div><!--/.inner-->

        <div id="coupon_area" class="pc">
          <a href="<?php echo home_url('/'); ?>common/img/top/coupon_ticket.gif" class="ophv"><img src="<?php echo home_url('/'); ?>common/img/top/coupon.gif" height="265" width="76" alt="WEB限定クーポン" class="coupon" ></a>
        </div><!-- /#coupon_area -->
      </div><!--/.seciton #section01-->

      <section>
        <div class="section" id="section02">
          <div class="inner">
            <div class="unit">
              <h2><img src="<?php echo home_url('/'); ?>common/img/top/iyashinoyu.png" height="285" width="175" alt="緑に包まれた癒しの湯"></h2>
              <div class="txt_area">
                <p>千葉県成田市にあるスーパー銭湯崋の湯（華の湯）では、和の湯と洋の湯で男女日替わりで沢山のお風呂が楽しめます。</p>
                <p>成田山新勝寺や成田空港、成田ゆめ牧場などに遊びに来たら、崋の湯（華の湯）にお立ち寄りになり、お風呂と岩盤浴、ボディーケアで体を休めてお帰りください。</p>
                <p>食事処や無料休憩室もございます。緑に包まれた施設でごゆっくりとお寛ぎください。</p>
              </div><!--/.txt_area-->
            </div><!--/.unit-->
            <div class="youtube_area pc">
              <p class="youtube">
                <iframe width="418" height="245" src="https://www.youtube.com/embed/RZD93QdFQz0?rel=0" frameborder="0" allowfullscreen></iframe>
              </p>
              <p>※　箱むしサウナのご利用は終了させて頂きました</p>
            </div><!-- /.youtube_area -->
          </div><!--/.inner-->
          <div id="line_area" class="pc">
            <a href="<?php echo home_url('/'); ?>common/img/top/pdf/line_pop.pdf" class="ophv"><img src="<?php echo home_url('/'); ?>common/img/top/line.gif" height="327" width="76" alt="LINE QRコード" class="line"></a>
          </div><!-- /#coupon_area -->
        </div><!--/.section #section02-->
      </section>

      <section>
        <div class="section" id="section03">
          <div class="inner">
            <h2 class="pc"><img src="<?php echo home_url('/'); ?>common/img/top/sec03_ttl.png" height="47" width="997" alt="華の湯だより"></h2>
            <h2 class="rsp"><img src="<?php echo home_url('/'); ?>common/img/rsp/sec03_ttl.png" height="47" width="128" alt="華の湯だより"></h2>
            <ul>
<?php if(have_posts()): while(have_posts()): the_post(); //投稿取得メインループ ?>
              <li>
<?php //ターム取得
  if(get_post_type() == 'news' || get_post_type() == 'facility'):
    $terms = get_the_terms( $post->ID, array('newscategory','facilitycategory') );
      foreach($terms as $term){
        $term_name = $term->name;
        $term_slug = $term->slug;
      }
?>
                <h3><img src="<?php echo home_url('/'); ?>common/img/news/tag_<?php echo $term_slug; ?>.jpg" height="112" width="141" alt="<?php echo $term_name; ?>"></h3>
<?php elseif(get_post_type() == 'bath'): ?>
                <h3><img src="<?php echo home_url('/'); ?>common/img/news/tag_daily.jpg" height="112" width="141" alt="日替わり湯スケジュール案内"></h3>
<?php endif; //ターム取得ここまで ?>
                <div class="txt_area">
                  <p class="date"><?php the_time('Y.n.j'); ?></p>
                  <p class="txt"><a href="<?php echo home_url('/'); ?>news/#entry<?php the_ID(); ?>"><?php the_title(); ?></a></p>
                </div><!--/.txt_area-->
              </li>

<?php endwhile; endif; //投稿取得メインループここまで ?>
            </ul>
            <p class="ichiran_btn pc"><a href="<?php echo home_url('/'); ?>news/" class="ophv">崋の湯だより一覧　＞</a></p>
          </div><!--/.inner-->
        </div><!--/.section #section03-->
      </section>

      <div class="section" id="section04">
        <div class="inner">
          <ul>
            <li><a href="<?php echo home_url('/'); ?>facility/hogushidoko/"><img src="<?php echo home_url('/'); ?>common/img/top/body_care02.png" height="225" width="225" alt="ボディーケアほぐし処" class="ophv"></a></li>
            <li><a href="<?php echo home_url('/'); ?>facility/akasuri/"><img src="<?php echo home_url('/'); ?>common/img/top/korea02.png" height="225" width="225" alt="本場韓国式あかすり" class="ophv"></a></li>
            <li><a href="<?php echo home_url('/'); ?>facility/bali/"><img src="<?php echo home_url('/'); ?>common/img/top/bali02.png" height="225" width="225" alt="バリ式エステ" class="ophv"></a></li>
            <li><a href="<?php echo home_url('/'); ?>facility/restaurant/"><img src="<?php echo home_url('/'); ?>common/img/top/restaurant.png" height="225" width="225" alt="お食事処崋" class="ophv"></a></li>
          </ul>
        </div><!--/.inner-->
      </div><!--/.section #section03-->

      <div class="section" id="section05">
        <div class="inner">
          <ul>
            <li class="access">
              <section>
                <h2>アクセス</h2>
                <div class="txt_area">
                  <p>東関東自動車道富里ICより約3km</p>
                  <p>京成本線「公津の杜」駅より徒歩約8分</p>
                </div><!--/.txt_area-->
                <p class="detail_btn"><a href="<?php echo home_url('/'); ?>access/" class="ophv">詳細はこちら</a></p>
              </section>
            </li>

            <li class="t_price">
              <section>
                <h2>ご利用料金</h2>
                <div class="txt_area">
                  <p>大人（中学生以上） 820円</p>
                  <p>子供（4～11才） 410円</p>
                  <p>幼児（3才以下） 100円 </p>
                </div><!--/.txt_area-->
                <p class="detail_btn"><a href="<?php echo home_url('/'); ?>price/" class="ophv">詳細はこちら</a></p>
              </section>
            </li>

            <li class="midokoro">
              <section>
                <h2>近隣の見どころ</h2>
                <div class="txt_area">
                  <p>観光にお買い物</p>
                  <p>近隣の見どころをご紹介します</p>
                </div><!--/.txt_area-->
                <p class="detail_btn"><a href="<?php echo home_url('/'); ?>access#section02" class="ophv">詳細はこちら</a></p>
              </section>
            </li>
          </ul>
        </div><!--/.inner-->
      </div><!--/.section #section03-->
    </div><!--/.contents #top-->
  </main>

<?php get_footer(); ?>