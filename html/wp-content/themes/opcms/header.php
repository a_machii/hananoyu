<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
<?php
  global $this_pagetitle;
  global $this_page_keywd;
  global $this_page_desc;
?>
  <meta charset="<?php bloginfo('charset'); ?>">
  <title><?php if($this_pagetitle) : echo $this_pagetitle. ' | '; endif; ?>千葉県成田市のスーパー銭湯と岩盤浴｜崋の湯（華の湯）</title>
  <meta name="description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_pagetitle) : echo $this_pagetitle. ' - '; endif; ?>千葉県成田市にあるスーパー銭湯崋の湯（華の湯）で人口の温泉につかって、日帰り温泉はいかがですか。成田山新勝寺や空港、成田ゆめ牧場などに遊びに来たら、お風呂と岩盤浴で体を休めてください。" >
  <meta name="keywords" content="<?php if($this_page_keywd) : echo $this_page_keywd. ','; elseif($this_pagetitle) : echo $this_pagetitle. ','; endif; ?>スーパー銭湯,岩盤浴,帰り温泉,千葉,成田,崋の湯,華の湯" >
  <meta name="format-detection" content="telephone=no,address=no,email=no">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <link rel="stylesheet" href="<?php echo home_url('/'); ?>common/css/style.css">
  <!--&#91;if lt IE 9&#93;>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <!&#91;endif&#93;-->

  <!--google analyticsの記述-->
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-22985851-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  </script>

  <?php wp_head(); ?>
</head>

<body>
  <header>
    <div id="header">
      <div class="inner">
        <h1 class="pc">千葉県成田市のスーパー銭湯と岩盤浴は崋の湯（華の湯）へ</h1>
        <h1 class="rsp"><a href="<?php echo home_url('/'); ?>"><img src="<?php echo home_url('/'); ?>common/img/logo.png" height="79" width="167" alt="千葉県成田市のスーパー銭湯と岩盤浴は崋の湯（華の湯）へ"></a></h1>
          <div class="unit pc">
             <p class="logo"><a href="<?php echo home_url('/'); ?>"><img src="<?php echo home_url('/'); ?>common/img/logo.png" height="79" width="167" alt="崋の湯（華の湯）"></a></p>
             <p class="tel"><img src="<?php echo home_url('/'); ?>common/img/tel.png" height="79" width="190" alt="電話番号"></p>
          </div><!--/.unit-->
        <ul class="pc">
          <li><a href="" class="ophv">日本語</a></li>
          <li><a href="" class="ophv">English</a></li>
          <li><a href="" class="ophv">中文 繫体字</a></li>
          <li><a href="" class="ophv">中文 簡体字</a></li>
          <li><a href="" class="ophv">한국어</a></li>
        </ul>

        <!--rspのみ表示-->
        <p id="menu_btn" class="rsp"><img src="<?php echo home_url('/'); ?>common/img/rsp/menu_btn.jpg" height="61" width="67" alt="メニューボタン"></p>
      </div><!--/.inner-->
    </div><!--/#header-->

<?php if(is_home()): ?>
    <div id="slider" class="pc">
      <div class="inner">
        <ul>
          <li><img src="<?php echo home_url('/'); ?>common/img/top/main01.jpg" height="500" width="1100" alt="メイン画像1"></li>
          <li><img src="<?php echo home_url('/'); ?>common/img/top/main02.jpg" height="500" width="1100" alt="メイン画像2"></li>
          <li><img src="<?php echo home_url('/'); ?>common/img/top/main03.jpg" height="500" width="1100" alt="メイン画像2"></li>
          <li><img src="<?php echo home_url('/'); ?>common/img/top/main04.jpg" height="500" width="1100" alt="メイン画像4"></li>
        </ul>
        <div id="slideFilterL" class="pc"></div>
        <div id="slideFilterR" class="pc"></div>
        <p class="catchcopy pc"><img src="<?php echo home_url('/'); ?>common/img/top/catchcopy.png" height="80" width="435" alt="憩いの空間癒しの湯"></p>
      </div><!--/.inner-->
    </div><!--/slider-->

    <div id="slider_rsp" class="rsp">
      <div class="inner">
        <ul>
          <li><img src="<?php echo home_url('/'); ?>common/img/rsp/main01.jpg" height="500" width="1100" alt="メイン画像1"></li>
          <li><img src="<?php echo home_url('/'); ?>common/img/rsp/main02.jpg" height="500" width="1100" alt="メイン画像2"></li>
          <li><img src="<?php echo home_url('/'); ?>common/img/rsp/main03.jpg" height="500" width="1100" alt="メイン画像3"></li>
          <li><img src="<?php echo home_url('/'); ?>common/img/rsp/main04.jpg" height="500" width="1100" alt="メイン画像4"></li>
        </ul>
      </div><!--/.inner-->
    </div><!--/slider-->

<?php else: ?>
    <div id="low-mimg">
      <h2><span><?php echo $this_pagetitle; ?></span></h2>
    </div><!-- /#low-mimg -->
<?php endif; ?>

    <nav>
      <div id="gnav">
        <div class="inner">
          <ul id="menu01" class="pc">
            <li><a href="<?php echo home_url('/'); ?>" class="gnav01">ホーム</a></li>
            <li><a href="<?php echo home_url('/'); ?>bath" class="gnav02">お風呂のご案内</a></li>
            <li><a href="<?php echo home_url('/'); ?>facility" class="gnav03">施設のご案内</a></li>
            <li><a href="<?php echo home_url('/'); ?>price" class="gnav04">ご利用料金について</a></li>
            <li><a href="<?php echo home_url('/'); ?>access" class="gnav05">アクセス</a></li>
            <li><a href="<?php echo home_url('/'); ?>faq" class="gnav06">よくある質問</a></li>
          </ul>

          <ul id="menu" class="rsp">
            <li><a href="<?php echo home_url('/'); ?>" class="gnav01">ホーム</a></li>
            <li><a href="<?php echo home_url('/'); ?>bath" class="gnav02">お風呂のご案内</a></li>
            <li><a href="<?php echo home_url('/'); ?>facility" class="gnav03">施設のご案内</a></li>
            <li><a href="<?php echo home_url('/'); ?>price" class="gnav04">ご利用料金について</a></li>
            <li><a href="<?php echo home_url('/'); ?>access" class="gnav05">アクセス</a></li>
            <li><a href="<?php echo home_url('/'); ?>faq" class="gnav06">よくある質問</a></li>
          </ul>
        </div><!--/.inner-->
      </div><!--/#gnav-->
    </nav>
  </header>