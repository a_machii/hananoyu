<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'aa213kbvhe_wordpress');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'aa213kbvhe');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'KgSrxTAa');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'r+P(0#uDe<YcX+fHvY_=9Xn|~)83^] =z#]8C#mDJ}.xS(-S)RewNh8k[v:k-Cz9');
define('SECURE_AUTH_KEY',  ';t:Yf1A2K>36)a3pEIM&}Z<j?(kYeD-S=U$|#eR+X8~RdR8.bo_`Jr^w?lWRo-us');
define('LOGGED_IN_KEY',    'p ~p@.G-|v7XT0|tuhFyAyOMkX&c-|nj3+EawY+8wPuu3l-)-BcDQB4C]c>ZC?-i');
define('NONCE_KEY',        'Yyb||j_`Qjh>f87h3/;&I:kV+C:-Da^4FK@$i},E&VSv?X*k!0:%do_&{Yv*HNM1');
define('AUTH_SALT',        'J9@P+]Sds8wpNlV*F&@n-9$`{wug57~!`6qR b|S=&a6nI>|ZF-}Wf[&wNo_z;>F');
define('SECURE_AUTH_SALT', ';zE0j#}K$1+vFxfp/)#mrI0c-rczO91Cj>}G2VK.tf2+7&iH>92%||VvOY^PRg0Y');
define('LOGGED_IN_SALT',   'vP F,##1o397G+p)>-=.|%JX|[92hrz!4Ip2^*OFo>&H%+%kLi=o8|p{Lo||Z<s$');
define('NONCE_SALT',       'K~oHC[T)b6p$_m[5SZ@%$eluT.=KgS^X, 8R[Of|%6pIn`gM{bQePbg#N52C4aC_');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
